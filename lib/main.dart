import 'package:flutter/material.dart';
import 'package:crudsqlite_app/database/database.dart';

final String dbName = "contactos1";

void main() => runApp(CrudSqlite());

class CrudSqlite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Ejemplo CRUD Flutter",
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Implementar createState
    return _HomeState();
  }
  //_HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Ciudad> _list;
  DatabaseHelper _databaseHelper;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CRUD en Flutter'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              //Definir este boton para agregar
              insert(context);
            },
          )
        ],
      ),
      body: _getBody(),
    );
  }

  void insert(BuildContext context) {
    Ciudad nNombre = new Ciudad();
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Nuevo"),
          content: TextField(
            onChanged: (value) {
              nNombre.titulo = value;
            },
            decoration: InputDecoration(labelText: "Titulo"),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancelar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Guardar"),
              onPressed: () {
                Navigator.of(context).pop();
                _databaseHelper.insert(nNombre).then((value) {
                  updateList();
                });
              },
            ),
          ],
        );
      },
    );
  }

  void onDeletedRequest(int index) {
    Ciudad ciudad = _list[index];
    _databaseHelper.delete(ciudad).then((value) {
      setState(() {
        _list.removeAt(index);
      });
    });
  }

  void onUpdatedRequest(int index) {
    Ciudad nNombre = _list[index];
    final controller = TextEditingController(text: nNombre.titulo);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Modificar"),
          content: TextField(
            controller: controller,
            onChanged: (value) {
              nNombre.titulo = value;
            },
            decoration: InputDecoration(labelText: "Titulo"),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancelar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Actualizar"),
              onPressed: () {
                Navigator.of(context).pop();
                _databaseHelper.update(nNombre).then((value) {
                  updateList();
                });
              },
            ),
          ],
        );
      }
    );
  }

  Widget _getBody() {
    if (_list == null) {
      return CircularProgressIndicator();
    } else if (_list.length == 0) {
      return Text("No hay Datos");
    } else {
      return ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, index) {
          Ciudad ciudad = _list[index];
          return CiudadWidget(ciudad, onDeletedRequest, onUpdatedRequest, index);
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _databaseHelper = new DatabaseHelper();
    updateList();
  }

  void updateList() {
    _databaseHelper.getList().then((resultList) {
      setState(() {
        _list = resultList;
      });
    });
  }
}

// ayuda a definir las funcioes que vamos a pedir a los atributs
typedef OnDeleted = void Function(int index);
typedef OnUpdate = void Function(int index);

class CiudadWidget extends StatelessWidget {
  final Ciudad ciudad;
  final OnDeleted onDeleted;
  final OnUpdate onUpdate;
  final int index;
  CiudadWidget(this.ciudad, this.onDeleted, this.onUpdate, this.index);

  @override
  Widget build(BuildContext context) {
    return Dismissible (
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(ciudad.titulo),
            ),
            IconButton(
              icon: Icon(
                Icons.edit,
                size: 30
              ),
              onPressed: () {
                this.onUpdate(index);
              },
            ),
          ],
        ),
      ),
      key: Key("${ciudad.id}"),
      onDismissed: (direction) {
        onDeleted(this.index);
      },
    );
  }
}
