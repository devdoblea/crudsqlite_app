import 'package:sqflite/sqflite.dart';

abstract class TablaElement {
  // declara variables de los campos que va a llevar la tabla
  int id;
  final String tablaNombre;

  // Instancio las variables
  TablaElement(this.id, this.tablaNombre);

  // creo el metodo
  void createTable(Database db);
  Map<String, dynamic> toMap();
}

class Ciudad extends TablaElement {
  // declaro que una tabla se va a llamar de la siguiente forma
  static final String tableName = "ciudad";
  // declaro las variables
  String titulo;

  // instancio las variables 
  Ciudad({this.titulo, id}):super(id, tableName);
  factory Ciudad.fromMap(Map<String, dynamic> map) {
    return Ciudad(titulo: map["titulo"], id: map["_id"]);
  }

  // creo el metodo que va a usar esas variables
  @override
  void createTable(Database db) {
    db.rawUpdate("CREATE TABLE $tableName(_id integer primary key autoincrement, titulo varchar(30))");
  }

  @override
  Map<String, dynamic> toMap() {
    // elementos de la base de datos
    //Map<String, dynamic> toMap(); tambien se puede escribir 
    var map = <String, dynamic>{"titulo": this.titulo};
    
    if(this.id != null) {
      map["_id"] = id;
    }

    return map;
  }
}

// nombre del archivo deonde se va a guardar la base de datos
final String dbFileName = "crud.db";

// clase que mantiene la conexion a la base de datos
class DatabaseHelper {
  // ayuda a que exista solo una instancia de la conexion
  // es decir que solo haya una sola conexion a la base de datos
  static final DatabaseHelper _instance = new DatabaseHelper._internal();
  factory DatabaseHelper() => _instance;
  DatabaseHelper._internal();

  Database _database;

  Future<Database> get db async {
    if (_database != null) {
      return _database;
    }

    _database = await open(); // se obliga a que termine de abrir open()

    return _database;
  }

    // conexion a la base de datos
  Future<Database> open() async {
    try{
      String databasesPath = await getDatabasesPath();
      String path = "$databasesPath/$dbFileName";
      var db = await openDatabase(
        path, 
        version: 1,
        // si la base de datos no existe se ejecuta la siguiente linea de comando
        onCreate: (Database database, int version) async { 
          new Ciudad().createTable(database);
        });
      return db;
    } catch(e) {
      print(e.toString());
    }

    return null;
  }

  // Future de tipo lista que devuelve una lista de los datos que estan en la base de datos
  Future<List<Ciudad>> getList() async {
    Database dbClient = await db;

    List<Map> maps = await dbClient.query(
      Ciudad.tableName,
      columns: ["_id", "titulo"]
    );
    //print("result sql: ${maps.lenght} $db");
    return maps.map((i) => new Ciudad.fromMap(i)).toList();
  }

  // funcion para agregar un registro
  Future<TablaElement> insert(TablaElement element) async {
    var dbClient = await db;
    element.id = await dbClient.insert(element.tablaNombre, element.toMap());
    print("nuevo Id ${element.id}");
    return element;
  }

  // funcion para borrar un registro
  Future<int> delete(TablaElement element) async {
    var dbClient = await db;
    return await dbClient.delete(element.tablaNombre, where: "_id = ?", whereArgs: [element.id]);
    
  }

  Future<int> update(TablaElement element) async {
    var dbClient = await db;
    return await dbClient.update(element.tablaNombre, element.toMap(), where: "_id = ?", whereArgs: [element.id]);
  }
}