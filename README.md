CRUD app hecha en Flutter para consultar, registrar, actualizar y borrar datos
haciendo uso de sqLite para el guardado de datos a través del Back-end.

Se puede eliminar el registro haciendo uso del "Gesture" "Deslizar hacia un lado"
y la app borra el registro tanto de la base de datos como de la lista (haciendo uso de "SetState" y "dispose")

![Pantalla Inicial](assets/images/Screenshot_2019-11-15-19-31-31.png)

# crudsqlite_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
